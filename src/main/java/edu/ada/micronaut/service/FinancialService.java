package edu.ada.micronaut.service;

public interface FinancialService {

    Object getYahooFinancialData(String stock_index);
    Object getIEXFinancialData(String stock_index);
}
