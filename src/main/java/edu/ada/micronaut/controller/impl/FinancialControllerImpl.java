package edu.ada.micronaut.controller.impl;

import edu.ada.micronaut.controller.FinancialController;
import edu.ada.micronaut.service.FinancialService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

@Controller("/financial") @Secured(SecurityRule.IS_AUTHENTICATED)
public class FinancialControllerImpl implements FinancialController {

    protected static final Logger logger = LoggerFactory.getLogger(FinancialControllerImpl.class);

    @Inject
    private FinancialService financialService;

    @Override
    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public Object getFinancialData(
            @QueryValue("provider") String financial_data_provider,
            @QueryValue("stock_index") String stock_index
    ) {
        logger.info("User requested " + stock_index + " from " + financial_data_provider);
        
        if(financial_data_provider.equals("yahoo")) {
            return "{\n\t\"" + stock_index + "\": " + financialService.getYahooFinancialData(stock_index.toUpperCase()) + "\n}";
        } else if(financial_data_provider.equals("iex")) {
            return "{\n\t\"" + stock_index + "\": " + financialService.getIEXFinancialData(stock_index.toUpperCase()) + "\n}";
        } else {
            return HttpResponse.badRequest("Provider Name Is Invalid");
        }
    }
    
    @Override
    @Get(value = "/batch")
    @Produces(MediaType.APPLICATION_JSON)
    public Object getBatchFinancialData(
            @QueryValue("provider") String financial_data_provider,
            @QueryValue String stock_indices)
    {
        logger.info("User requested " + stock_indices + " from " + financial_data_provider);
        
        StringBuilder resultJSON = new StringBuilder("{\n\t");
        List<String> stocks = Arrays.asList(stock_indices.split(","));
        
        // I used Reflection here.
        // Used this for learning: https://stackoverflow.com/questions/4138527/how-to-call-a-java-method-using-a-variable-name
        Method serviceMethod;
    
        try {
            if(financial_data_provider.equals("yahoo")) {
                serviceMethod = financialService.getClass().getMethod("getYahooFinancialData", String.class);
            } else if(financial_data_provider.equals("iex")) {
                serviceMethod = financialService.getClass().getMethod("getIEXFinancialData", String.class);
            } else {
                return HttpResponse.badRequest("Provider Name Is Invalid");
            }
            
            for (int i = 0; i < stocks.size(); i++) {
                Object financialData = serviceMethod.invoke(financialService,stocks.get(i).toUpperCase());
                resultJSON.append("\"");
                resultJSON.append(stocks.get(i));
                resultJSON.append("\":");
                resultJSON.append(financialData);
                if (i != stocks.size() - 1) resultJSON.append(",\n");
            }
    
            resultJSON.append("\n}");
            
            return resultJSON;
            
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            return HttpResponse.badRequest(e.getMessage());
        }
    }
}
