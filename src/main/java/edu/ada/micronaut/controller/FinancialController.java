package edu.ada.micronaut.controller;

import java.util.List;

public interface FinancialController {

    Object getFinancialData(String financial_data_provider, String stock_index);
    Object getBatchFinancialData(String financial_data_provider, String stock_indices);
}
